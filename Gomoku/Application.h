#pragma once
#include <Windows.h>
#include "Constants.h"

class ScreenManager;

class Application
{
protected:
	 shared_ptr<sf::RenderWindow> window;
	 shared_ptr<sf::Font> font;
	 shared_ptr<ScreenManager> screenManager;
public:
	Application(sf::ContextSettings settings);
	~Application();

	shared_ptr<sf::RenderWindow> getWindow();
	shared_ptr<sf::Font> getFont();
	void setScreenManager(shared_ptr<ScreenManager> sm);
	shared_ptr<ScreenManager> getScreenManager();
};

