#pragma once
#include <unordered_map>
#include <functional>
#include "GameState.h"

class MinMaxTree
{
protected:
	static const int DEPTH = 8;

	static int makeTree(GameState& state, int depth, int alpha, int beta, bool maxi);
public:
	static pair<int, int> nextMove(GameState& state);
};

