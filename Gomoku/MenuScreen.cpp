#include "MenuScreen.h"

MenuScreen::MenuScreen(shared_ptr<Application> context) : Screen(context)
{
	drawables.push_back(shared_ptr<UIElement>(new Text(context, sf::Vector2f(SCREEN_WIDTH / 2, 20), APP_TITLE, 100)));
}

MenuScreen::~MenuScreen()
{
}

shared_ptr<TextButton> MenuScreen::addButton(std::string buttonText, OnClickListener* listener)
{
	shared_ptr<TextButton> button(new TextButton(context, sf::Vector2f(SCREEN_WIDTH / 2, 80 + controllers.size() * 70 + drawables.size() * 70), buttonText));
	button->setOnClickListener(listener);
	controllers.push_back(button);
	return button;
}