#pragma once
#include "Player.h"

class LocalPlayer : public Player
{
public:
	LocalPlayer(BoardField playerColor);
	~LocalPlayer();

	bool isWaitingForLocalMove() override;
};

