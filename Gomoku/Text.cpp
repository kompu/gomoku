#include "Text.h"


Text::Text(shared_ptr<Application> context, sf::Vector2f pos, string str, int fontSize) : UIElement(context, pos)
{
	origin = pos;
	shared_ptr<sf::Font> font = context->getFont();
	text.setFont(*font);
	text.setString(str);
	text.setCharacterSize(fontSize);
	text.setColor(COLOR_CATSKILL);
	text.setStyle(sf::Text::Bold);
	text.setPosition(origin.x - text.getGlobalBounds().width / 2, origin.y);
}


Text::~Text()
{
}

void Text::draw()
{
	shared_ptr<sf::RenderWindow> window = context->getWindow();
	window->draw(text);
}

bool Text::contains(sf::Vector2f mouse)
{
	return text.getGlobalBounds().contains(mouse);
}

string Text::getText()
{
	return text.getString();
}

void Text::setText(string str)
{
	text.setString(str);
	text.setPosition(origin.x - text.getGlobalBounds().width / 2, origin.y);
}