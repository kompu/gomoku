#pragma once
#include "MenuScreen.h"
#include "TextButton.h"
#include "ScreenManager.h"
#include "OnDialogResult.h"
#include "DialogScreen.h"
#include "GameScreen.h"
#include "MultiplayerScreen.h"
#include "ComputerPlayer.h"

class MainMenuScreen : public MenuScreen, public OnDialogResult
{
protected:
	shared_ptr<TextButton> singlePlayerButton;
	shared_ptr<TextButton> hotSeatButton;
	shared_ptr<TextButton> multiPlayerButton;
	shared_ptr<TextButton> exitButton;
public:
	MainMenuScreen(shared_ptr<Application> context);
	~MainMenuScreen();

	void onClick(Controller* controller) override;
	void onDialogResult(string title, string ret) override;
};