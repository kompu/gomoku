#include "MultiplayerScreen.h"

MultiplayerScreen::MultiplayerScreen(shared_ptr<Application> context) : MenuScreen(context)
{
	searchButton = addButton("Search for games", this);
	connectToIpButton = addButton("Connect to IP", this);
	hostButton = addButton("Host new game", this);
	cancelButton = addButton("Cancel", this);
}

MultiplayerScreen::~MultiplayerScreen()
{
}

void MultiplayerScreen::onClick(Controller* controller)
{
	if (controller == searchButton.get())
	{
		context->getScreenManager()->addScreen(new MultiplayerSearchScreen(context));
	}
	else if (controller == connectToIpButton.get())
	{
		context->getScreenManager()->addScreen(new MultiplayerConnectScreen(context));
	}
	else if (controller == hostButton.get())
	{
		vector<string> buttonsText{ "White", "Black", "Cancel" };
		context->getScreenManager()->addScreen(new DialogScreen(context, "Choose your color:", buttonsText, this));
	}
	else if (controller == cancelButton.get())
	{
		context->getScreenManager()->removeLastScreen();
	}
}

void MultiplayerScreen::onDialogResult(string title, string ret)
{
	auto screenManager = context->getScreenManager();
	screenManager->removeLastScreen();
	if (ret.compare("White") == 0)
	{
		screenManager->addScreen(new HostScreen(context, BoardField::WHITE));
	}
	else if (ret.compare("Black") == 0)
	{
		screenManager->addScreen(new HostScreen(context, BoardField::BLACK));
	}
}