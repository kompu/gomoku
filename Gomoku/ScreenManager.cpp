#include "ScreenManager.h"


ScreenManager::ScreenManager(shared_ptr<Application> context) : View(context)
{
	window = context->getWindow();
	addScreen(new MainMenuScreen(context));
}

ScreenManager::~ScreenManager()
{
}

void ScreenManager::run()
{
	while (window->isOpen())
	{
		sf::Event event;
		while (window->pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::MouseButtonReleased:
				screens.back()->mouseClick(event.key.code);
				break;
			case sf::Event::KeyPressed:
				screens.back()->keyPressed(event.key.code);
				break;
			case sf::Event::MouseWheelMoved:
				screens.back()->mouseWheel(event.mouseWheel);
				break;
			case sf::Event::TextEntered:
				if (event.text.unicode < 256)
					screens.back()->textEntered(event.text.unicode);
				break;
			case sf::Event::Closed:
				window->close();
				break;
			}
		}
		render();
	}
}

void ScreenManager::addScreen(Screen* screen)
{
	screens.push_back(unique_ptr<Screen>(screen));
}

void ScreenManager::render()
{
	window->clear(COLOR_GOTHIC);
	screens.back()->render();
	window->display();
}

void ScreenManager::removeLastScreen()
{
	screens.pop_back();
	if (screens.size() == 0)
		addScreen(new MainMenuScreen(context));
}

void ScreenManager::removePreviousScreen()
{
	if (screens.size() > 1)
	{
		auto last = move(screens.back());
		screens.pop_back();
		screens.pop_back();
		screens.push_back(move(last));
	}
}

void ScreenManager::removeAllScreens()
{
	screens.clear();
	addScreen(new MainMenuScreen(context));
}