#include "MinMaxTree.h"

int MinMaxTree::makeTree(GameState& state, int depth, int alpha, int beta, bool maxi)
{
	if (depth == 0)
		return state.getValue(state.getPlayerColor());
	auto moves = state.getBestMoves();
	if (maxi)
	{
		for (auto it : moves)
		{
			state.makeMove(it.first, it.second);
			alpha = max(alpha, makeTree(state, depth - 1, alpha, beta, !maxi));
			state.undoMove(it.first, it.second);
			if (beta <= alpha)
				break;
		}
		return alpha;
	}
	else
	{
		for (auto it : moves)
		{
			state.makeMove(it.first, it.second);
			beta = min(beta, makeTree(state, depth - 1, alpha, beta, !maxi));
			state.undoMove(it.first, it.second);
			if (beta <= alpha)
				break;
		}
		return beta;
	}
	return state.getValue(state.getPlayerColor());
}

pair<int, int> MinMaxTree::nextMove(GameState& state)
{
	auto moves = state.getBestMoves();
	if (moves.size() == 1)
		return (*moves.begin());
	pair<int, int> move;
	int value, alpha = -INF;
	for (auto it : moves)
	{
		state.makeMove(it.first, it.second);
		value = makeTree(state, DEPTH - 1, alpha, INF, false);
		if (value > alpha)
		{
			alpha = value;
			move = it;
		}
		state.undoMove(it.first, it.second);
	}
	return move;
}