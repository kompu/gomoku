#include "GameBoardController.h"

GameBoardController::GameBoardController(shared_ptr<Application> context, sf::Vector2f pos) : Controller(context, pos), boardShape(sf::Vector2f(540, 540)), background(sf::Vector2f(SCREEN_HEIGHT, SCREEN_HEIGHT))
{
	background.setFillColor(COLOR_TAN);
	background.setPosition(pos - sf::Vector2f(30, 30));
	boardShape.setFillColor(COLOR_TAN);
	boardShape.setPosition(pos);

	shared_ptr<sf::Font> font = context->getFont();
	for (int i = 0; i < BOARD_SIZE; ++i)
	{
		char c = 'A' + i;
		string str(1, c);
		columnNames[i] = sf::Text(str, *font, 25);
		columnNames[i].setPosition(sf::Vector2f(42 + i * 36, 0));
		columnNames[i].setColor(COLOR_CATSKILL);
	}
	for (int i = 0; i < BOARD_SIZE; ++i)
	{
		string str = to_string(BOARD_SIZE - i);
		rowNames[i] = sf::Text(str, *font, 25);
		rowNames[i].setPosition(sf::Vector2f(5, 32 + i * 36));
		rowNames[i].setColor(COLOR_CATSKILL);
	}
}

GameBoardController::~GameBoardController()
{
}

void GameBoardController::draw()
{
	shared_ptr<sf::RenderWindow> window = context->getWindow();
	window->draw(background);
	window->draw(boardShape);

	for (int i = 0; i < BOARD_SIZE + 1; ++i)
	{
		sf::Vertex line[] = { 
			sf::Vertex(sf::Vector2f(0, i * 36) + pos, sf::Color::Black), 
			sf::Vertex(sf::Vector2f(36 * BOARD_SIZE, i * 36) + pos, sf::Color::Black) };
		window->draw(line, 2, sf::Lines);
	}
	for (int i = 0; i < BOARD_SIZE + 1; ++i)
	{
		sf::Vertex line[] = {
			sf::Vertex(sf::Vector2f(i * 36, 0) + pos, sf::Color::Black),
			sf::Vertex(sf::Vector2f(i * 36, 36 * BOARD_SIZE) + pos, sf::Color::Black) };
		window->draw(line, 2, sf::Lines);
	}

	sf::CircleShape white(17);
	white.setOrigin(sf::Vector2f(17, 17));
	white.setFillColor(sf::Color::White);
	white.setOutlineThickness(1);
	white.setOutlineColor(sf::Color::Black);
	sf::CircleShape black(white);
	black.setFillColor(sf::Color::Black);

	for (int i = 0; i < BOARD_SIZE; ++i)
	{
		for (int j = 0; j < BOARD_SIZE; ++j)
		{
			switch (board.getField(i, j))
			{
			case BoardField::WHITE:
				white.setPosition(sf::Vector2f(18 + j * 36, 18 + i * 36) + pos);
				window->draw(white);
				break;
			case BoardField::BLACK:
				black.setPosition(sf::Vector2f(18 + j * 36, 18 + i * 36) + pos);
				window->draw(black);
				break;
			}
		}
	}

	for (int i = 0; i < BOARD_SIZE; ++i)
	{
		sf::Transform columnTransform;
		columnTransform.translate(sf::Vector2f(0, 570));
		sf::Transform rowTransform;
		rowTransform.translate(sf::Vector2f(570, 0));

		window->draw(columnNames[i]);
		window->draw(columnNames[i], columnTransform);
		window->draw(rowNames[i]);
		window->draw(rowNames[i], rowTransform);
	}
}

bool GameBoardController::contains(sf::Vector2f mouse)
{
	return boardShape.getGlobalBounds().contains(mouse);
}

void GameBoardController::mouseClick(sf::Keyboard::Key key)
{
	shared_ptr<sf::RenderWindow> window = context->getWindow();
	sf::Vector2f mouse(sf::Mouse::getPosition(*window));
	mouse -= pos;
	lastI = (int)mouse.y / 36;
	lastJ = (int)mouse.x / 36;
	if (listener != nullptr && board.getField(lastI, lastJ) == BoardField::EMPTY)
		listener->onClick(this);
}

pair<int, int> GameBoardController::getLastMove()
{
	return make_pair(lastI, lastJ);
}

bool GameBoardController::makeMove(int i, int j)
{
	if (board.getField(i, j) == BoardField::EMPTY)
	{
		board.makeMove(i, j);
		return true;
	}
	else
	{
		return false;
	}
	
}

GameState& GameBoardController::getBoard()
{
	return board;
}