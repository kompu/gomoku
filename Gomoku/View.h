#pragma once
#include "Constants.h"
#include "Application.h"

class View
{
protected:
	shared_ptr<Application> context;
public:
	View(shared_ptr<Application> context);
	virtual ~View();
};

