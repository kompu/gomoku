#include "Controller.h"

Controller::Controller(shared_ptr<Application> context, sf::Vector2f pos) : UIElement(context, pos)
{
	listener = nullptr;
}

Controller::~Controller()
{
}

void Controller::setOnClickListener(OnClickListener* onClickListener)
{
	listener = onClickListener;
}

void Controller::mouseClick(sf::Keyboard::Key key)
{
	if (listener != nullptr)
	{
		listener->onClick(this);
	}
}