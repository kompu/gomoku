#pragma once
#include "Constants.h"
#include "UIElement.h"
#include "OnClickListener.h"

class Controller : public UIElement
{
protected:
	OnClickListener* listener;
public:
	Controller(shared_ptr<Application> context, sf::Vector2f pos);
	~Controller();

	void setOnClickListener(OnClickListener* onClickController);
	void virtual mouseClick(sf::Keyboard::Key key);
};