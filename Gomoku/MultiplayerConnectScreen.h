#pragma once
#include "MenuScreen.h"
#include "ScreenManager.h"

class MultiplayerConnectScreen : public MenuScreen
{
protected:
	shared_ptr<Text> ipAddressText;
	shared_ptr<TextButton> connectButton;
	shared_ptr<TextButton> cancelButton;
public:
	MultiplayerConnectScreen(shared_ptr<Application> context);
	~MultiplayerConnectScreen();

	void onClick(Controller* controller) override;
	void keyPressed(sf::Keyboard::Key key) override;
};