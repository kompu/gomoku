#include "NetworkException.h"

NetworkException::NetworkException(string errorMessage)
{
	this->errorMessage = errorMessage;
}

NetworkException::~NetworkException()
{
}

const char* NetworkException::what() const
{
	return errorMessage.c_str();
}