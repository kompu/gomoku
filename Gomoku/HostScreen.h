#pragma once
#include <SFML/Network.hpp>
#include "MenuScreen.h"
#include "ScreenManager.h"
#include "LocalPlayer.h"
#include "NetworkPlayer.h"

class HostScreen : public MenuScreen
{
protected:
	sf::TcpListener listener;
	sf::UdpSocket broadcaster;
	unique_ptr<sf::TcpSocket> connectionSocket;
	shared_ptr<TextButton> cancelButton;
	BoardField playerColor;
public:
	HostScreen(shared_ptr<Application> context, BoardField playerColor);
	~HostScreen();

	void render() override;
	void onClick(Controller* controller) override;
};

