#pragma once
#include "UIElement.h"

class TextBox :	public UIElement
{
private:
	sf::Vector2f size;
	int position, lastPosition, listCapacity;
	vector<sf::Text> list;
public:
	TextBox(shared_ptr<Application> context, sf::Vector2f pos, sf::Vector2f size);
	~TextBox();

	void draw() override;
	bool contains(sf::Vector2f mouse) override;

	string addText(string str, sf::Color color);
	void changePosition(int change);
};

