#include "MultiplayerSearchScreen.h"

MultiplayerSearchScreen::MultiplayerSearchScreen(shared_ptr<Application> context) : MenuScreen(context)
{
	cancelButton = MenuScreen::addButton("Cancel", this);

	listener.setBlocking(false);
	listener.bind(BROADCAST_PORT);
}

MultiplayerSearchScreen::~MultiplayerSearchScreen()
{
	listener.unbind();
}

void MultiplayerSearchScreen::render()
{
	Screen::render();
	sf::Packet packet;
	sf::IpAddress sender;
	unsigned short port;
	if (hosts.size() < 5 && listener.receive(packet, sender, port) == sf::Socket::Done)
	{
		string ip = sender.toString();
		if (hosts.find(ip) == hosts.end())
		{
			sf::Uint8 playerColor;
			packet >> playerColor;
			hosts[ip] = (BoardField)playerColor;
			addButton(ip, this);
		}
	}
}

void MultiplayerSearchScreen::onClick(Controller* controller)
{
	if (controller == cancelButton.get())
	{
		context->getScreenManager()->removeLastScreen();
	}
	else
	{
		TextButton* button = dynamic_cast<TextButton*>(controller);
		unique_ptr<sf::TcpSocket> connectionSocket(new sf::TcpSocket());
		string ip = button->getString();

		if (connectionSocket->connect(ip, GAME_PORT) == sf::Socket::Done)
		{
			sf::Uint8 message;
			sf::Packet packet;
			connectionSocket->receive(packet);
			packet >> message;
			BoardField playerColor = (BoardField)message;

			auto screenManager = context->getScreenManager();
			if (playerColor == BoardField::WHITE)
			{
				screenManager->addScreen(new GameScreen(context, new LocalPlayer(BoardField::BLACK), new NetworkPlayer(BoardField::WHITE, move(connectionSocket))));
			}
			else
			{
				screenManager->addScreen(new GameScreen(context, new NetworkPlayer(BoardField::BLACK, move(connectionSocket)), new LocalPlayer(BoardField::WHITE)));
			}
		}
	}
}

shared_ptr<TextButton> MultiplayerSearchScreen::addButton(std::string buttonText, OnClickListener* listener)
{
	controllers.pop_back();
	shared_ptr<TextButton> button = MenuScreen::addButton(buttonText, listener);
	cancelButton = MenuScreen::addButton("Cancel", this);
	return button;
}