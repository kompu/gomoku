#pragma once
#include <SFML/Network.hpp>
#include "Player.h"
#include "NetworkException.h"

class NetworkPlayer : public Player
{
protected:
	unique_ptr<sf::TcpSocket> connectionSocket;
	pair<int, int> lastMove;
	int awakeInterval;
public:
	NetworkPlayer(BoardField playerColor, unique_ptr<sf::TcpSocket> connectionSocket);
	~NetworkPlayer();

	bool isWaitingForLocalMove() override;
	void setOnPlayerListener(OnPlayerListener* listener) override;
	pair<int, int> getLastMove() override;
	void makeMove(pair<int, int> move) override;
	void clock() override;
	void sendMessage(string msg) override;
};