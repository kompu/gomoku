#include "Enums.h"

BoardField operator!(BoardField color)
{
	switch (color)
	{
	case BoardField::EMPTY:
		return BoardField::EMPTY;
	case BoardField::WHITE:
		return BoardField::BLACK;
	case BoardField::BLACK:
		return BoardField::WHITE;
	case BoardField::OUT_OF_RANGE:
		return BoardField::OUT_OF_RANGE;
	}
	return BoardField::EMPTY;
}

GomokuDirection operator!(GomokuDirection dir)
{
	switch (dir)
	{
	case GomokuDirection::N:
		return GomokuDirection::S;
	case GomokuDirection::NE:
		return GomokuDirection::SW;
	case GomokuDirection::E:
		return GomokuDirection::W;
	case GomokuDirection::SE:
		return GomokuDirection::NW;
	case GomokuDirection::S:
		return GomokuDirection::N;
	case GomokuDirection::SW:
		return GomokuDirection::NE;
	case GomokuDirection::W:
		return GomokuDirection::E;
	case GomokuDirection::NW:
		return GomokuDirection::SE;
	}
	return GomokuDirection::N;
}