#pragma once
#include "Constants.h"
#include "OnPlayerListener.h"

class Player
{
protected:
	BoardField playerColor;
	OnPlayerListener* listener;
public:
	Player(BoardField playerColor);
	virtual ~Player();
	
	BoardField getPlayerColor();
	virtual bool isWaitingForLocalMove() = 0;
	virtual void setOnPlayerListener(OnPlayerListener* listener);
	virtual pair<int, int> getLastMove();
	virtual void makeMove(pair<int, int> move);
	virtual void clock();
	virtual void sendMessage(string msg);
};