#pragma once
#include "Screen.h"
#include "TextButton.h"
#include "OnClickListener.h"

class MenuScreen : public Screen, public OnClickListener
{
protected:
	virtual shared_ptr<TextButton> addButton(std::string buttonText, OnClickListener* listener);
public:
	MenuScreen(shared_ptr<Application> context);
	~MenuScreen();
};

