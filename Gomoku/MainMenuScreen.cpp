#include "MainMenuScreen.h"

MainMenuScreen::MainMenuScreen(shared_ptr<Application> context) : MenuScreen(context)
{
	singlePlayerButton = addButton("Single Player", this);
	hotSeatButton = addButton("Hot Seat", this);
	multiPlayerButton = addButton("Multiplayer", this);
	exitButton = addButton("Exit", this);
}

MainMenuScreen::~MainMenuScreen()
{
}

void MainMenuScreen::onClick(Controller* controller)
{
	if (controller == singlePlayerButton.get())
	{
		vector<string> buttonsText{ "White", "Black", "Cancel" };
		context->getScreenManager()->addScreen(new DialogScreen(context, "Choose your color:", buttonsText, this));
	}
	else if (controller == hotSeatButton.get())
	{
		context->getScreenManager()->addScreen(new GameScreen(context, new LocalPlayer(BoardField::BLACK), new LocalPlayer(BoardField::WHITE)));
	}
	else if (controller == multiPlayerButton.get())
	{
		context->getScreenManager()->addScreen(new MultiplayerScreen(context));
	}
	else if (controller == exitButton.get())
	{
		context->getWindow()->close();
	}
}

void MainMenuScreen::onDialogResult(string title, string ret)
{
	auto screenManager = context->getScreenManager();
	screenManager->removeLastScreen();
	if (ret.compare("White") == 0)
	{
		screenManager->addScreen(new GameScreen(context, new ComputerPlayer(BoardField::BLACK), new LocalPlayer(BoardField::WHITE)));
	}
	else if (ret.compare("Black") == 0)
	{
		screenManager->addScreen(new GameScreen(context, new LocalPlayer(BoardField::BLACK), new ComputerPlayer(BoardField::WHITE)));
	}
}