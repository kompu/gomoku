#pragma once
#include "Constants.h"
#include "UIElement.h"

class Text : public UIElement
{
protected:
	sf::Vector2f origin;
	sf::Text text;
public:
	Text(shared_ptr<Application> context, sf::Vector2f pos, string str, int fontSize = 50);
	~Text();
	void draw() override;
	bool contains(sf::Vector2f mouse) override;
	string getText();
	void setText(string str);
};

