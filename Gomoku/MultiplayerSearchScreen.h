#pragma once
#include <SFML/Network.hpp>
#include <map>
#include "MenuScreen.h"
#include "ScreenManager.h"

class MultiplayerSearchScreen : public MenuScreen
{
protected:
	sf::UdpSocket listener;
	shared_ptr<TextButton> cancelButton;
	map<string, BoardField> hosts;
public:
	MultiplayerSearchScreen(shared_ptr<Application> context);
	~MultiplayerSearchScreen();

	void render() override;
	void onClick(Controller* controller) override;
	shared_ptr<TextButton> addButton(std::string buttonText, OnClickListener* listener) override;
};

