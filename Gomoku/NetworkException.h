#pragma once
#include <exception>
#include "Constants.h"

class NetworkException : public exception
{
protected:
	string errorMessage;
public:
	NetworkException(string errorMessage);
	~NetworkException();

	const char* what() const override;
};

