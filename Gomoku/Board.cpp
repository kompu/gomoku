#include "Board.h"

Board::Board()
{
	for (int i = 0; i < BOARD_SIZE; ++i)
	{
		for (int j = 0; j < BOARD_SIZE; ++j)
		{
			board[i][j] = BoardField::EMPTY;
		}
	}
}

Board::~Board()
{
}

BoardField Board::getField(int i, int j)
{
	if (i < 0 || j < 0 || i >= BOARD_SIZE || j >= BOARD_SIZE)
		return BoardField::OUT_OF_RANGE;
	else
		return board[i][j];
}

BoardField Board::getNeighbour(int i, int j, GomokuDirection dir)
{
	auto pos = getCoordinates(i, j, dir);
	if (pos.first == -1)
		return BoardField::OUT_OF_RANGE;
	else
		return board[pos.first][pos.second];
}

BoardField Board::getNeighbour(pair<int, int> pos, GomokuDirection dir)
{
	return getNeighbour(pos.first, pos.second, dir);
}

BoardField Board::getNeighbour(int i, int j, GomokuDirection dir, int length)
{
	auto pos = getCoordinates(i, j, dir, length);
	if (pos.first == -1)
		return BoardField::OUT_OF_RANGE;
	else
		return board[pos.first][pos.second];
}

BoardField Board::getNeighbour(pair<int, int> pos, GomokuDirection dir, int length)
{
	return getNeighbour(pos.first, pos.second, dir, length);
}

pair<int, int> Board::getCoordinates(int i, int j, GomokuDirection dir)
{
	switch (dir)
	{
	case GomokuDirection::N:
		--i;
		break;
	case GomokuDirection::NE:
		--i;
		++j;
		break;
	case GomokuDirection::E:
		++j;
		break;
	case GomokuDirection::SE:
		++i;
		++j;
		break;
	case GomokuDirection::S:
		++i;
		break;
	case GomokuDirection::SW:
		++i;
		--j;
		break;
	case GomokuDirection::W:
		--j;
		break;
	case GomokuDirection::NW:
		--i;
		--j;
		break;
	}
	if (i < 0 || i >= BOARD_SIZE || j < 0 || j >= BOARD_SIZE)
		return make_pair(-1, -1);
	else
		return make_pair(i, j);
}

pair<int, int> Board::getCoordinates(pair<int, int> pos, GomokuDirection dir)
{
	return getCoordinates(pos.first, pos.second, dir);
}

pair<int, int> Board::getCoordinates(int i, int j, GomokuDirection dir, int length)
{
	switch (dir)
	{
	case GomokuDirection::N:
		i -= length;
		break;
	case GomokuDirection::NE:
		i -= length;
		j += length;
		break;
	case GomokuDirection::E:
		j += length;
		break;
	case GomokuDirection::SE:
		i += length;
		j += length;
		break;
	case GomokuDirection::S:
		i += length;
		break;
	case GomokuDirection::SW:
		i += length;
		j -= length;
		break;
	case GomokuDirection::W:
		j -= length;
		break;
	case GomokuDirection::NW:
		i -= length;
		j -= length;
		break;
	}
	if (i < 0 || i >= BOARD_SIZE || j < 0 || j >= BOARD_SIZE)
		return make_pair(-1, -1);
	else
		return make_pair(i, j);
}

pair<int, int> Board::getCoordinates(pair<int, int> pos, GomokuDirection dir, int length)
{
	return getCoordinates(pos.first, pos.second, dir, length);
}

bool Board::isConnected(int i, int j, int radius)
{
	if (radius == 0 || board[i][j] != BoardField::EMPTY)
		return false;
	for (int d = 0; d < 8; ++d)
	{
		auto neighbour = getNeighbour(i, j, (GomokuDirection)d, radius);
		if (neighbour == BoardField::BLACK || neighbour == BoardField::WHITE)
			return true;
	}
	return isConnected(i, j, radius - 1);
}