#include "MessageController.h"

MessageController::MessageController(shared_ptr<Application> context, sf::Vector2f pos) : UIElement(context, pos), 
messageList(context, pos + sf::Vector2f(10, 65), sf::Vector2f(SCREEN_WIDTH - pos.x - 20, SCREEN_HEIGHT - 120))
{
	player = BoardField::BLACK;
	listener = nullptr;

	backgroundRect.setPosition(pos);
	backgroundRect.setSize(sf::Vector2f(SCREEN_WIDTH - pos.x, SCREEN_HEIGHT));
	backgroundRect.setFillColor(COLOR_NOMAD);

	turnMessageRect.setPosition(pos + sf::Vector2f(10, 10));
	turnMessageRect.setSize(sf::Vector2f(SCREEN_WIDTH - pos.x - 20, 50));
	turnMessageRect.setFillColor(COLOR_JET);

	messageListRect.setPosition(pos + sf::Vector2f(10, 70));
	messageListRect.setSize(sf::Vector2f(SCREEN_WIDTH - pos.x - 20, SCREEN_HEIGHT - 120));
	messageListRect.setFillColor(COLOR_JET);

	inputMessageRect.setPosition(pos + sf::Vector2f(10, SCREEN_HEIGHT - 40));
	inputMessageRect.setSize(sf::Vector2f(SCREEN_WIDTH - pos.x - 20, 30));
	inputMessageRect.setFillColor(COLOR_BLUE);

	auto font = context->getFont();
	turnText.setFont(*font);
	turnText.setPosition(pos + sf::Vector2f(15, 10));
	turnText.setCharacterSize(40);
	turnText.setColor(sf::Color::Black);
	turnText.setString("Black's turn");

	messageText.setFont(*font);
	messageText.setPosition(pos + sf::Vector2f(15, SCREEN_HEIGHT - 40));
	messageText.setCharacterSize(20);
	messageText.setString("_");
}

MessageController::~MessageController()
{

}

void MessageController::draw()
{
	shared_ptr<sf::RenderWindow> window = context->getWindow();
	window->draw(backgroundRect);
	window->draw(turnMessageRect);
	window->draw(messageListRect);
	window->draw(turnText);
	window->draw(inputMessageRect);
	window->draw(messageText);
	messageList.draw();
}

bool MessageController::contains(sf::Vector2f mouse)
{
	return backgroundRect.getGlobalBounds().contains(mouse.x, mouse.y);
}

void MessageController::makeMove(pair<int, int> move)
{
	string msg;
	if (player == BoardField::BLACK)
	{
		player = BoardField::WHITE;
		turnText.setColor(COLOR_CATSKILL);
		turnText.setString("White's turn");
		msg = "Black ";
	}
	else
	{
		player = BoardField::BLACK;
		turnText.setColor(sf::Color::Black);
		turnText.setString("Black's turn");
		msg = "White ";
	}
	char c = 'A' + move.second;
	msg += string(1, c);
	msg += to_string(BOARD_SIZE - move.first);
	messageList.addText(msg, COLOR_GOTHIC);
}

void MessageController::mouseWheelMoved(int delta)
{
	messageList.changePosition(-delta);
}

void MessageController::textEntered(char c)
{
	string str = messageText.getString();
	str.pop_back();
	if (c == '\b')
	{
		if (messageString.size() > 0)
		{
			messageString.pop_back();
			str = messageString + "_";
			messageText.setString(str);
			while (messageText.getLocalBounds().width > inputMessageRect.getSize().x - 5)
			{
				str.erase(0, 1);
				messageText.setString(str);
			}
		}
	}
	else if (c == '\r')
	{
		if (!messageString.empty())
		{
			postMessage(messageString, writingPlayer);
			if (listener != nullptr)
			{
				listener->onSendMessage(messageString);
			}
			messageString = "";
			str = "_";
			messageText.setString(str);
		}
	}
	else
	{
		messageString += c;
		str += c;
		str += '_';
		messageText.setString(str);
		while (messageText.getLocalBounds().width > inputMessageRect.getSize().x - 5)
		{
			str.erase(0, 1);
			messageText.setString(str);
		}
	}
}

void MessageController::gameEnded(GameResult result)
{
	string msg;
	switch (result)
	{
	case GameResult::WHITE_WIN:
		turnText.setColor(COLOR_CATSKILL);
		msg = "White has won!";
		break;
	case GameResult::BLACK_WIN:
		turnText.setColor(sf::Color::Black);
		msg = "Black has won!";
		break;
	case GameResult::DRAW:
		turnText.setColor(COLOR_GOTHIC);
		msg = "Draw";
		break;
	}
	turnText.setString(msg);
	messageList.addText(msg, COLOR_GOTHIC);
}

void MessageController::postMessage(string message, BoardField playerColor)
{
	auto messageColor = playerColor == BoardField::BLACK ? sf::Color::Black : sf::Color::White;
	do
	{
		message = messageList.addText(message, messageColor);
	} while (!message.empty());
}

void MessageController::setOnSendMessageListener(OnSendMessageListener *listener)
{
	this->listener = listener;
}

void MessageController::setWritingPlayer(BoardField color)
{
	writingPlayer = color;
	messageText.setColor(color == BoardField::BLACK ? sf::Color::Black : sf::Color::White);
}