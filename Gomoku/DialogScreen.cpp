#include "DialogScreen.h"


DialogScreen::DialogScreen(shared_ptr<Application> context, string dialogTitle, vector<string> buttonsText, OnDialogResult* listener) : MenuScreen(context)
{
	this->dialogTitle = dialogTitle;
	if (!dialogTitle.empty())
	{
		drawables.push_back(shared_ptr<UIElement>(new Text(context, sf::Vector2f(SCREEN_WIDTH / 2, 150), dialogTitle, 50)));
	}
	this->buttonsText = buttonsText;
	this->listener = listener;
	for (auto it : buttonsText)
	{
		buttons.push_back(addButton(it, this));
	}
}

DialogScreen::~DialogScreen()
{
}

void DialogScreen::onClick(Controller* controller)
{
	for (auto it : buttons)
	{
		if (it.get() == controller)
		{
			if (listener != nullptr)
				listener->onDialogResult(dialogTitle, it->getString());
			break;
		}
	}
}
