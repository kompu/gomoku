#include "TextBox.h"

TextBox::TextBox(shared_ptr<Application> context, sf::Vector2f pos, sf::Vector2f size) : UIElement(context, pos)
{
	this->size = size;
	position = lastPosition = 0;
	listCapacity = size.y / 20;
}

TextBox::~TextBox()
{
}

void TextBox::draw()
{
	shared_ptr<sf::RenderWindow> window = context->getWindow();
	for (int i = 0; i < listCapacity && i + position < list.size(); ++i)
	{
		window->draw(list[i + position]);
	}
}

bool TextBox::contains(sf::Vector2f mouse)
{
	return false;
}

string TextBox::addText(string str, sf::Color color)
{
	auto font = context->getFont();
	sf::Text text(str, *font, 20);
	text.setColor(color);
	auto inserted = str;
	while (text.getLocalBounds().width > this->size.x)
	{
		auto pos = inserted.find_last_of(" ");
		if (pos != string::npos)
		{
			inserted = inserted.substr(0, pos);
		}
		else
		{
			inserted.pop_back();
		}
		text.setString(inserted);
	}
	list.push_back(text);
	position = lastPosition = list.size() <= listCapacity ? 0 : list.size() - listCapacity;
	changePosition(0);
	if (inserted.size() < str.size() && str[inserted.size()] == ' ')
		return str.substr(inserted.size() + 1, str.size() - inserted.size() - 1);
	else
		return str.substr(inserted.size(), str.size() - inserted.size());
}

void TextBox::changePosition(int change)
{
	position += change;
	if (position > lastPosition)
	{
		position = lastPosition;
	}
	if (position < 0)
	{
		position = 0;
	}
	for (int i = 0; i < listCapacity && i + position < list.size(); ++i)
	{
		list[i + position].setPosition(pos + sf::Vector2f(0, 20 * i));
	}
}