#include "Player.h"

Player::Player(BoardField playerColor)
{
	this->playerColor = playerColor;
}

Player::~Player()
{
}

BoardField Player::getPlayerColor()
{
	return playerColor;
}

void Player::setOnPlayerListener(OnPlayerListener* listener)
{
}

pair<int, int> Player::getLastMove()
{
	return make_pair(0, 0);
}

void Player::makeMove(pair<int, int> move)
{
}

void Player::clock()
{
}

void Player::sendMessage(string msg)
{
}