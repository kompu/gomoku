#pragma once
#include "Screen.h"
#include "GameBoardController.h"
#include "LocalPlayer.h"
#include "OnPlayerListener.h"
#include "ScreenManager.h"
#include "PauseMenuScreen.h"
#include "MessageController.h"
#include "OnSendMessageListener.h"

class GameScreen : public Screen, public OnClickListener, public OnPlayerListener, public OnDialogResult, public OnSendMessageListener
{
protected:
	shared_ptr<GameBoardController> gameBoard;
	unique_ptr<Player> players[2];
	shared_ptr<MessageController> messageController;
	int currentPlayerId;
	bool isPlaying;

	void changePlayer();
public:
	GameScreen(shared_ptr<Application> context, Player* firstPlayer, Player* secondPlayer);
	~GameScreen();

	void onClick(Controller* controller) override;
	void mouseWheel(sf::Event::MouseWheelEvent wheel) override;
	void keyPressed(sf::Keyboard::Key key) override;
	void textEntered(char c) override;
	void onPlayerMove(Player* controller) override;
	void onPlayerMessage(Player* controller, string message) override;
	void onDialogResult(string title, string ret) override;
	void onSendMessage(string message) override;
	void render() override;
};