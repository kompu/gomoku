#pragma once
#include "Constants.h"

class OnDialogResult
{
public:
	virtual ~OnDialogResult() { }

	virtual void onDialogResult(string title, string ret) = 0;
};