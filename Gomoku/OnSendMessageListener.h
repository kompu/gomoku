#pragma once
#include "Constants.h"

class OnSendMessageListener
{
public:
	virtual ~OnSendMessageListener() { }

	virtual void onSendMessage(string message) = 0;
};