#include "Application.h"


Application::Application(sf::ContextSettings settings) :
	window(new sf::RenderWindow(sf::VideoMode(SCREEN_WIDTH, SCREEN_HEIGHT), APP_TITLE, sf::Style::Titlebar | sf::Style::Close, settings)),
	font(new sf::Font())
{
	window->setVerticalSyncEnabled(true);
	if (!font->loadFromFile("font.ttf"))
	{
		window->close();
		string message = "Couldn't find font.ttf";
		string caption = "Error";
		MessageBoxA(NULL, message.c_str(), caption.c_str(), MB_OK);
	}
}

Application::~Application()
{
}

shared_ptr<sf::RenderWindow> Application::getWindow()
{
	return window;
}

shared_ptr<sf::Font> Application::getFont()
{
	return font;
}

void Application::setScreenManager(shared_ptr<ScreenManager> sm)
{
	screenManager = sm;
}

shared_ptr<ScreenManager> Application::getScreenManager()
{
	return screenManager;
}