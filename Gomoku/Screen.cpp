#include "Screen.h"


Screen::Screen(shared_ptr<Application> context) : View(context)
{
}


Screen::~Screen()
{
}

void Screen::render()
{
	for (auto it = drawables.begin(); it != drawables.end(); ++it)
	{
		(*it)->draw();
	}
	for (auto it = controllers.begin(); it != controllers.end(); ++it)
	{
		(*it)->draw();
	}
}

void Screen::mouseClick(sf::Keyboard::Key key)
{
	shared_ptr<sf::RenderWindow> window = context->getWindow();
	sf::Vector2f mouse(sf::Mouse::getPosition(*window));
	for (auto it = controllers.begin(); it != controllers.end(); ++it)
	{
		if ((*it)->contains(mouse))
		{
			(*it)->mouseClick(key);
			return;
		}
	}
}

void Screen::mouseWheel(sf::Event::MouseWheelEvent wheel)
{
}

void Screen::keyPressed(sf::Keyboard::Key key)
{
}

void Screen::textEntered(char c)
{
}