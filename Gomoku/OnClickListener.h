#pragma once
#include "Constants.h"

class Controller;

class OnClickListener
{
public:
	virtual ~OnClickListener() { }

	virtual void onClick(Controller* controller) = 0;
};