#pragma once
#include "MenuScreen.h"
#include "TextButton.h"
#include "OnDialogResult.h"
#include "ScreenManager.h"

class DialogScreen : public MenuScreen
{
protected:
	string dialogTitle;
	vector<string> buttonsText;
	vector<shared_ptr<TextButton>> buttons;
	OnDialogResult* listener;
public:
	DialogScreen(shared_ptr<Application> context, string dialogTitle, vector<string> buttonsText, OnDialogResult* listener);
	~DialogScreen();

	void onClick(Controller* controller) override;
};

