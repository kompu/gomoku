#include "GameState.h"

GameState::GameState() : crossingLines(BOARD_SIZE, vector<vector<int>>(BOARD_SIZE))
{
	currentPlayer = BoardField::BLACK;

	allLines.reserve(4 * (BOARD_SIZE - 4) * (BOARD_SIZE - 2));
	for (int i = 0; i < BOARD_SIZE; ++i)
	{
		for (int j = 0; j < BOARD_SIZE - 4; ++j)
		{
			allLines.push_back(Line(make_pair(i, j), GomokuDirection::E));
			for (int x = 0; x < 5; ++x)
			{
				auto move = getCoordinates(i, j, GomokuDirection::E, x);
				crossingLines[move.first][move.second].push_back(allLines.size() - 1);
			}
		}
	}
	for (int i = 0; i < BOARD_SIZE - 4; ++i)
	{
		for (int j = 0; j < BOARD_SIZE; ++j)
		{
			allLines.push_back(Line(make_pair(i, j), GomokuDirection::S));
			for (int x = 0; x < 5; ++x)
			{
				auto move = getCoordinates(i, j, GomokuDirection::S, x);
				crossingLines[move.first][move.second].push_back(allLines.size() - 1);
			}
		}
	}
	for (int i = 0; i < BOARD_SIZE - 4; ++i)
	{
		for (int j = 0; j < BOARD_SIZE - 4; ++j)
		{
			allLines.push_back(Line(make_pair(i, j), GomokuDirection::SE));
			for (int x = 0; x < 5; ++x)
			{
				auto move = getCoordinates(i, j, GomokuDirection::SE, x);
				crossingLines[move.first][move.second].push_back(allLines.size() - 1);
			}
		}
	}
	for (int i = 4; i < BOARD_SIZE; ++i)
	{
		for (int j = 0; j < BOARD_SIZE - 4; ++j)
		{
			allLines.push_back(Line(make_pair(i, j), GomokuDirection::NE));
			for (int x = 0; x < 5; ++x)
			{
				auto move = getCoordinates(i, j, GomokuDirection::NE, x);
				crossingLines[move.first][move.second].push_back(allLines.size() - 1);
			}
		}
	}
}

GameState::~GameState()
{
}

BoardField GameState::getPlayerColor()
{
	return currentPlayer;
}

vector<pair<int, int>> GameState::getMoves()
{
	vector<pair<int, int>> res;
	if (currentPlayer == BoardField::BLACK)
	{
		if (!blackFour.empty())
		{
			auto id = *(blackFour.begin());
			for (int i = 0; i < 5; ++i)
			{
				auto coord = getCoordinates(allLines[id].pos, allLines[id].dir, i);
				if (board[coord.first][coord.second] == BoardField::EMPTY)
				{
					res.push_back(make_pair(coord.first, coord.second));
					return res;
				}
			}
		}
		if (!whiteFour.empty())
		{
			auto id = *(whiteFour.begin());
			for (int i = 0; i < 5; ++i)
			{
				auto coord = getCoordinates(allLines[id].pos, allLines[id].dir, i);
				if (board[coord.first][coord.second] == BoardField::EMPTY)
				{
					res.push_back(make_pair(coord.first, coord.second));
					return res;
				}
			}
		}
		if (!whiteThree.empty())
		{
			map<pair<int, int>, int> moves;
			for (auto block : whiteThree)
			{
				for (int i = 0; i < 5; ++i)
				{
					auto coord = getCoordinates(allLines[block].pos, allLines[block].dir, i);
					if (board[coord.first][coord.second] == BoardField::EMPTY)
					{
						if (moves.find(coord) != moves.end())
							moves[coord]++;
						else
							moves[coord] = 1;
					}
				}
			}
			int maxi = 0;
			pair<int, int> move;
			for (auto block : moves)
			{
				if (block.second > maxi)
				{
					maxi = block.second;
					move = block.first;
				}
			}
			if (maxi > 1)
			{
				res.push_back(move);
				return res;
			}
		}
	}
	else
	{
		if (!whiteFour.empty())
		{
			auto id = *(whiteFour.begin());
			for (int i = 0; i < 5; ++i)
			{
				auto coord = getCoordinates(allLines[id].pos, allLines[id].dir, i);
				if (board[coord.first][coord.second] == BoardField::EMPTY)
				{
					res.push_back(make_pair(coord.first, coord.second));
					return res;
				}
			}
		}
		if (!blackFour.empty())
		{
			auto id = *(blackFour.begin());
			for (int i = 0; i < 5; ++i)
			{
				auto coord = getCoordinates(allLines[id].pos, allLines[id].dir, i);
				if (board[coord.first][coord.second] == BoardField::EMPTY)
				{
					res.push_back(make_pair(coord.first, coord.second));
					return res;
				}
			}
		}
		if (!blackThree.empty())
		{
			map<pair<int, int>, int> moves;
			for (auto block : blackThree)
			{
				for (int i = 0; i < 5; ++i)
				{
					auto coord = getCoordinates(allLines[block].pos, allLines[block].dir, i);
					if (board[coord.first][coord.second] == BoardField::EMPTY)
					{
						if (moves.find(coord) != moves.end())
							moves[coord]++;
						else
							moves[coord] = 1;
					}
				}
			}
			int maxi = 0;
			pair<int, int> move;
			for (auto block : moves)
			{
				if (block.second > maxi)
				{
					maxi = block.second;
					move = block.first;
				}
			}
			if (maxi > 1)
			{
				res.push_back(move);
				return res;
			}
		}
	}
	for (int i = 0; i < BOARD_SIZE; ++i)
	{
		for (int j = 0; j < BOARD_SIZE; ++j)
		{
			if (isConnected(i, j, 1))
				res.push_back(make_pair(i, j));
		}
	}
	if (res.empty())
		res.push_back(make_pair(BOARD_SIZE / 2, BOARD_SIZE / 2));
	return res;
}

vector<pair<int, int>> GameState::getBestMoves()
{
	auto moves = getMoves();
	if (moves.size() == 1)
		return moves;
	vector<pair<int, pair<int, int>>> sortedMoves;
	sortedMoves.reserve(moves.size());
	for (auto it : moves)
	{
		makeMove(it.first, it.second);
		sortedMoves.push_back(make_pair(getValue(!currentPlayer), it));
		undoMove(it.first, it.second);
	}
	sort(sortedMoves.begin(), sortedMoves.end());
	moves.clear();
	for (auto it = sortedMoves.rbegin(); it != sortedMoves.rend() && moves.size() < BEST_MOVES_COUNT; ++it)
	{
		moves.push_back((*it).second);
	}
	return moves;
}

void GameState::evaluateLinesAt(int i, int j)
{
	for (auto it : crossingLines[i][j])
	{
		whiteTwo.erase(it);
		whiteThree.erase(it);
		whiteFour.erase(it);
		whiteFive.erase(it);
		blackTwo.erase(it);
		blackThree.erase(it);
		blackFour.erase(it);
		blackFive.erase(it);
		if (allLines[it].white == 0 && allLines[it].black != 0)
		{
			switch (allLines[it].black)
			{
			case 2:
				blackTwo.insert(it);
				break;
			case 3:
				blackThree.insert(it);
				break;
			case 4:
				blackFour.insert(it);
				break;
			case 5:
				blackFive.insert(it);
				break;
			}
		}
		else if (allLines[it].black == 0 && allLines[it].white != 0)
		{
			switch (allLines[it].white)
			{
			case 2:
				whiteTwo.insert(it);
				break;
			case 3:
				whiteThree.insert(it);
				break;
			case 4:
				whiteFour.insert(it);
				break;
			case 5:
				whiteFive.insert(it);
				break;
			}
		}
	}
}

void GameState::makeMove(int i, int j)
{
	board[i][j] = currentPlayer;
	++numberOfMoves;
	for (auto it : crossingLines[i][j])
	{
		if (currentPlayer == BoardField::BLACK)
			++allLines[it].black;
		else
			++allLines[it].white;
	}
	evaluateLinesAt(i, j);
	currentPlayer = !currentPlayer;
}

void GameState::undoMove(int i, int j)
{
	for (auto it : crossingLines[i][j])
	{
		if (board[i][j] == BoardField::BLACK)
			--allLines[it].black;
		else
			--allLines[it].white;
	}
	board[i][j] = BoardField::EMPTY;
	--numberOfMoves;
	evaluateLinesAt(i, j);
	currentPlayer = !currentPlayer;
}

int GameState::getValue(BoardField player)
{
	int white = 0, black = 0;
	if (!whiteFive.empty())
		return player == BoardField::BLACK ? -INF : INF;
	if (!blackFive.empty())
		return player == BoardField::BLACK ? INF : -INF;
	white += whiteTwo.size();
	white += whiteThree.size() * 4;
	white += whiteFour.size() * 8;
	black += blackTwo.size();
	black += blackThree.size() * 4;
	black += blackFour.size() * 8;
	int res = player == BoardField::BLACK ? black - white : white - black;
	return res;
}

GameResult GameState::gameState()
{
	if (!whiteFive.empty())
		return GameResult::WHITE_WIN;
	if (!blackFive.empty())
		return GameResult::BLACK_WIN;
	if (numberOfMoves == BOARD_SIZE * BOARD_SIZE)
		return GameResult::DRAW;
	return GameResult::PLAYING;
}

Line::Line() { }

Line::Line(pair<int, int> pos, GomokuDirection dir)
{
	this->pos = pos;
	this->dir = dir;
}