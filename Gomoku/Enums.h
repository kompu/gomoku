#pragma once

enum class BoardField
{
	EMPTY,
	WHITE,
	BLACK,
	OUT_OF_RANGE
};

enum class GomokuDirection
{
	N,
	NE,
	E,
	SE,
	S,
	SW,
	W,
	NW
};

enum class GameResult
{
	WHITE_WIN,
	BLACK_WIN,
	DRAW,
	PLAYING
};

enum class PacketMessage
{
	MOVE_MESSAGE,
	AWAKE_MESSAGE,
	STRING_MESSAGE
};

BoardField operator!(BoardField color);
GomokuDirection operator!(GomokuDirection dir);