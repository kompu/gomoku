#pragma once
#include "Text.h"
#include "Controller.h"

class TextButton : public Controller
{
protected:
	sf::Text text;
public:
	TextButton(shared_ptr<Application> context, sf::Vector2f pos, string str, int fontSize = 50);
	~TextButton();

	void draw() override;
	bool contains(sf::Vector2f mouse) override;
	string getString();
};

