#pragma once
#include <vector>
#include "View.h"
#include "UIElement.h"
#include "Controller.h"

class Screen : public View
{
protected:
	vector<shared_ptr<UIElement>> drawables;
	vector<shared_ptr<Controller>> controllers;
public:
	Screen(shared_ptr<Application> context);
	~Screen();
	virtual void render();
	virtual void mouseClick(sf::Keyboard::Key key);
	virtual void mouseWheel(sf::Event::MouseWheelEvent wheel);
	virtual void keyPressed(sf::Keyboard::Key key);
	virtual void textEntered(char c);
};