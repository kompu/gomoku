#pragma once
#include <future>
#include "Player.h"
#include "MinMaxTree.h"

class ComputerPlayer : public Player
{
protected:
	pair<int, int> lastMove;
	GameState board;
	bool turn, waiting;
	future<pair<int, int>> task;
public:
	ComputerPlayer(BoardField playerColor);
	~ComputerPlayer();

	bool isWaitingForLocalMove() override;
	void setOnPlayerListener(OnPlayerListener* listener) override;
	pair<int, int> getLastMove() override;
	void makeMove(pair<int, int> move) override;
	void clock() override;
};

