#include "MultiplayerConnectScreen.h"

MultiplayerConnectScreen::MultiplayerConnectScreen(shared_ptr<Application> context) : MenuScreen(context)
{
	ipAddressText = unique_ptr<Text>(new Text(context, sf::Vector2f(SCREEN_WIDTH / 2, 150), "_"));
	drawables.push_back(ipAddressText);
	connectButton = addButton("Connect", this);
	cancelButton = addButton("Cancel", this);
}

MultiplayerConnectScreen::~MultiplayerConnectScreen()
{
}

void MultiplayerConnectScreen::onClick(Controller* controller)
{
	if (controller == connectButton.get())
	{
		unique_ptr<sf::TcpSocket> connectionSocket(new sf::TcpSocket());
		string ip = ipAddressText->getText();

		if (connectionSocket->connect(ip, GAME_PORT) == sf::Socket::Done)
		{
			sf::Uint8 message;
			sf::Packet packet;
			connectionSocket->receive(packet);
			packet >> message;
			BoardField playerColor = (BoardField)message;

			auto screenManager = context->getScreenManager();
			if (playerColor == BoardField::WHITE)
			{
				screenManager->addScreen(new GameScreen(context, new LocalPlayer(BoardField::BLACK), new NetworkPlayer(BoardField::WHITE, move(connectionSocket))));
			}
			else
			{
				screenManager->addScreen(new GameScreen(context, new NetworkPlayer(BoardField::BLACK, move(connectionSocket)), new LocalPlayer(BoardField::WHITE)));
			}
		}
	}
	else if (controller == cancelButton.get())
	{
		context->getScreenManager()->removeLastScreen();
	}
}

void MultiplayerConnectScreen::keyPressed(sf::Keyboard::Key key)
{
	string ipAddress = ipAddressText->getText();
	ipAddress.pop_back();
	switch (key)
	{
	case sf::Keyboard::Num0:
		ipAddress += '0';
		break;
	case sf::Keyboard::Num1:
		ipAddress += '1';
		break;
	case sf::Keyboard::Num2:
		ipAddress += '2';
		break;
	case sf::Keyboard::Num3:
		ipAddress += '3';
		break;
	case sf::Keyboard::Num4:
		ipAddress += '4';
		break;
	case sf::Keyboard::Num5:
		ipAddress += '5';
		break;
	case sf::Keyboard::Num6:
		ipAddress += '6';
		break;
	case sf::Keyboard::Num7:
		ipAddress += '7';
		break;
	case sf::Keyboard::Num8:
		ipAddress += '8';
		break;
	case sf::Keyboard::Num9:
		ipAddress += '9';
		break;
	case sf::Keyboard::Period:
		ipAddress += '.';
		break;
	case sf::Keyboard::BackSpace:
		if (!ipAddress.empty())
		{
			ipAddress.pop_back();
		}
		break;
	case sf::Keyboard::Return:
		onClick(connectButton.get());
		break;
	}
	ipAddressText->setText(ipAddress + "_");
}