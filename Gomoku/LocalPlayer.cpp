#include "LocalPlayer.h"

LocalPlayer::LocalPlayer(BoardField playerColor) : Player(playerColor)
{
	listener = nullptr;
}

LocalPlayer::~LocalPlayer()
{
}

bool LocalPlayer::isWaitingForLocalMove()
{
	return true;
}