#pragma once
#include "MenuScreen.h"
#include "ScreenManager.h"

class PauseMenuScreen :	public MenuScreen
{
protected:
	shared_ptr<TextButton> continueButton;
	shared_ptr<TextButton> mainMenuButton;
	shared_ptr<TextButton> exitButton;
public:
	PauseMenuScreen(shared_ptr<Application> context);
	~PauseMenuScreen();

	void onClick(Controller* controller) override;
	void keyPressed(sf::Keyboard::Key key) override;
};

