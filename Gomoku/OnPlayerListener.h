#pragma once

class Player;

class OnPlayerListener
{
public:
	virtual ~OnPlayerListener() { }

	virtual void onPlayerMove(Player* controller) = 0;
	virtual void onPlayerMessage(Player* controller, string message) = 0;
};