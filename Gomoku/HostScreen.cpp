#include "HostScreen.h"

HostScreen::HostScreen(shared_ptr<Application> context, BoardField playerColor) : MenuScreen(context), connectionSocket(new sf::TcpSocket())
{
	this->playerColor = playerColor;
	drawables.push_back(unique_ptr<UIElement>(new Text(context, sf::Vector2f(SCREEN_WIDTH / 2, 250), "Waiting for other player...", 40)));
	cancelButton = shared_ptr<TextButton>(new TextButton(context, sf::Vector2f(SCREEN_WIDTH / 2, 400), "Cancel"));
	cancelButton->setOnClickListener(this);
	controllers.push_back(cancelButton);

	listener.setBlocking(false);
	listener.listen(GAME_PORT);
}

HostScreen::~HostScreen()
{
	listener.close();
}

void HostScreen::render()
{
	Screen::render();
	sf::Packet packet;
	packet << (sf::Uint8)playerColor;
	if (listener.accept(*connectionSocket) == sf::Socket::Done)
	{
		listener.close();
		connectionSocket->send(packet);
		auto screenManager = context->getScreenManager();
		if (playerColor == BoardField::WHITE)
		{
			screenManager->addScreen(new GameScreen(context, new NetworkPlayer(BoardField::BLACK, move(connectionSocket)), new LocalPlayer(BoardField::WHITE)));
		}
		else
		{
			screenManager->addScreen(new GameScreen(context, new LocalPlayer(BoardField::BLACK), new NetworkPlayer(BoardField::WHITE, move(connectionSocket))));
		}
		screenManager->removePreviousScreen();
	}
	else
	{
		broadcaster.send(packet, sf::IpAddress::Broadcast, BROADCAST_PORT);
	}
}

void HostScreen::onClick(Controller* controller)
{
	if (controller == cancelButton.get())
	{
		context->getScreenManager()->removeLastScreen();
	}
}