#include <Windows.h>
#include <SFML/Graphics.hpp>
#include "Application.h"
#include "ScreenManager.h"

int WINAPI WinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPSTR, int)
{
	sf::ContextSettings settings;
	settings.antialiasingLevel = 8;

	shared_ptr<Application> app(new Application(settings));
	shared_ptr<ScreenManager> screenManager(new ScreenManager(app));
	app->setScreenManager(screenManager);

	screenManager->run();

	return 0;
}