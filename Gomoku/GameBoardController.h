#pragma once
#include "Controller.h"
#include "GameState.h"
#include "Text.h"

class GameBoardController : public Controller
{
protected:
	sf::RectangleShape boardShape;
	sf::RectangleShape background;
	sf::Text rowNames[BOARD_SIZE], columnNames[BOARD_SIZE];
	GameState board;
	int lastI, lastJ;
public:
	GameBoardController(shared_ptr<Application> context, sf::Vector2f pos);
	~GameBoardController();

	void draw() override;
	bool contains(sf::Vector2f mouse) override;
	void mouseClick(sf::Keyboard::Key key) override;
	pair<int, int> getLastMove();
	bool makeMove(int i, int j);
	GameState& getBoard();
};

