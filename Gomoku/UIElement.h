#pragma once
#include "View.h"

class UIElement : public View
{
protected:
	sf::Vector2f pos;
public:
	UIElement(shared_ptr<Application> context, sf::Vector2f pos);
	virtual ~UIElement();
	virtual void draw() = 0;
	virtual bool contains(sf::Vector2f mouse) = 0;
};

