#include "GameScreen.h"

GameScreen::GameScreen(shared_ptr<Application> context, Player* firstPlayer, Player* secondPlayer) : Screen(context), 
gameBoard(new GameBoardController(context, sf::Vector2f(30, 30))), messageController(new MessageController(context, sf::Vector2f(SCREEN_HEIGHT, 0)))
{
	gameBoard->setOnClickListener(this);
	controllers.push_back(gameBoard);
	drawables.push_back(messageController);
	players[0] = unique_ptr<Player>(firstPlayer);
	players[0]->setOnPlayerListener(this);
	players[1] = unique_ptr<Player>(secondPlayer);
	players[1]->setOnPlayerListener(this);
	currentPlayerId = 0;
	isPlaying = true;
	messageController->setOnSendMessageListener(this);
	auto player = dynamic_cast<LocalPlayer*>(players[0].get());
	if (player)
	{
		messageController->setWritingPlayer(player->getPlayerColor());
	}
	else
	{
		messageController->setWritingPlayer(players[1]->getPlayerColor());
	}
}

GameScreen::~GameScreen()
{
}

void GameScreen::onClick(Controller* controller)
{
	if (controller == gameBoard.get() && players[currentPlayerId]->isWaitingForLocalMove() && isPlaying)
	{
		auto move = gameBoard->getLastMove();
		messageController->makeMove(move);
		if (gameBoard->makeMove(move.first, move.second))
		{
			try
			{
				players[0]->makeMove(move);
				players[1]->makeMove(move);
			}
			catch (NetworkException& e)
			{
				vector<string> buttons = { "OK" };
				context->getScreenManager()->addScreen(new DialogScreen(context, e.what(), buttons, this));
			}
			changePlayer();
		}
	}
}

void GameScreen::onPlayerMove(Player* controller)
{
	auto move = players[currentPlayerId]->getLastMove();
	messageController->makeMove(move);
	if (gameBoard->makeMove(move.first, move.second))
	{
		changePlayer();
	}
}

void GameScreen::changePlayer()
{
	auto board = gameBoard->getBoard();
	auto res = board.gameState();
	if (res != GameResult::PLAYING)
	{
		string title;
		vector<string> buttons = { "OK" };
		switch (res)
		{
		case GameResult::BLACK_WIN:
			title = "Black has won!";
			break;
		case GameResult::WHITE_WIN:
			title = "White has won!";
			break;
		case GameResult::DRAW:
			title = "Draw!";
			break;
		}
		context->getScreenManager()->addScreen(new DialogScreen(context, title, buttons, this));
		isPlaying = false;
		messageController->gameEnded(res);
	}
	else
	{
		currentPlayerId = 1 - currentPlayerId;
		auto currentPlayer = dynamic_cast<LocalPlayer*>(players[currentPlayerId].get());
		if (currentPlayer)
		{
			messageController->setWritingPlayer(currentPlayer->getPlayerColor());
		}
	}
}

void GameScreen::keyPressed(sf::Keyboard::Key key)
{
	if (key == sf::Keyboard::Escape)
		context->getScreenManager()->addScreen(new PauseMenuScreen(context));
}

void GameScreen::textEntered(char c)
{
	messageController->textEntered(c);
}

void GameScreen::render()
{
	Screen::render();
	try
	{
		if (isPlaying)
		{
			players[0]->clock();
			players[1]->clock();
		}
	}
	catch (NetworkException& e)
	{
		vector<string> buttons = { "OK" };
		context->getScreenManager()->addScreen(new DialogScreen(context, e.what(), buttons, this));
	}
}

void GameScreen::onDialogResult(string title, string ret)
{
	if (title.compare("Connection error") == 0)
		context->getScreenManager()->removeAllScreens();
	else
		context->getScreenManager()->removeLastScreen();
}

void GameScreen::mouseWheel(sf::Event::MouseWheelEvent wheel)
{
	messageController->mouseWheelMoved(wheel.delta);
}

void GameScreen::onPlayerMessage(Player* controller, string message)
{
	messageController->postMessage(message, controller->getPlayerColor());
}

void GameScreen::onSendMessage(string message)
{
	players[0]->sendMessage(message);
	players[1]->sendMessage(message);
}