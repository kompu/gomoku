#pragma once
#include <set>
#include "Board.h"

struct Line
{
	pair<int, int> pos;
	GomokuDirection dir;
	int black = 0;
	int white = 0;

	Line();
	Line(pair<int, int> pos, GomokuDirection dir);
};

class GameState : public Board
{
protected:
	int const BEST_MOVES_COUNT = 10;

	BoardField currentPlayer;
	int numberOfMoves = 0;
	vector<Line> allLines;
	vector<vector<vector<int>>> crossingLines;
	set<int> blackTwo, blackThree, blackFour, blackFive, whiteTwo, whiteThree, whiteFour, whiteFive;

	void evaluateLinesAt(int i, int j);
public:
	GameState();
	~GameState();

	BoardField getPlayerColor();
	vector<pair<int, int>> getMoves();
	vector<pair<int, int>> getBestMoves();
	void makeMove(int i, int j);
	void undoMove(int i, int j);
	int getValue(BoardField player);
	GameResult gameState();
};

