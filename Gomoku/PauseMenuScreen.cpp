#include "PauseMenuScreen.h"

PauseMenuScreen::PauseMenuScreen(shared_ptr<Application> context) : MenuScreen(context)
{
	continueButton = addButton("Continue", this);
	mainMenuButton = addButton("Main Menu", this);
	exitButton = addButton("Exit", this);
}

PauseMenuScreen::~PauseMenuScreen()
{
}

void PauseMenuScreen::onClick(Controller* controller)
{
	if (controller == continueButton.get())
	{
		context->getScreenManager()->removeLastScreen();
	}
	else if (controller == mainMenuButton.get())
	{
		context->getScreenManager()->removeAllScreens();
	}
	else if (controller == exitButton.get())
	{
		context->getWindow()->close();
	}
}

void PauseMenuScreen::keyPressed(sf::Keyboard::Key key)
{
	if (key == sf::Keyboard::Escape)
		context->getScreenManager()->removeLastScreen();
}