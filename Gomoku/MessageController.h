#pragma once
#include "Controller.h"
#include "TextBox.h"
#include "OnSendMessageListener.h"

class MessageController : public UIElement
{
private:
	BoardField player, writingPlayer;
	string messageString;
	sf::RectangleShape backgroundRect;
	sf::RectangleShape turnMessageRect;
	sf::RectangleShape messageListRect;
	sf::RectangleShape inputMessageRect;
	sf::Text turnText;
	sf::Text messageText;
	TextBox messageList;
	OnSendMessageListener *listener;
public:
	MessageController(shared_ptr<Application> context, sf::Vector2f pos);
	~MessageController();

	void draw() override;
	bool contains(sf::Vector2f mouse) override;

	void makeMove(pair<int, int> move);
	void mouseWheelMoved(int delta);
	void textEntered(char c);
	void gameEnded(GameResult result);
	void setOnSendMessageListener(OnSendMessageListener *listener);
	void postMessage(string message, BoardField playerColor);
	void setWritingPlayer(BoardField color);
};

