#pragma once
#include <vector>
#include "Constants.h"

class Board
{
protected:
	BoardField board[BOARD_SIZE][BOARD_SIZE];
public:
	Board();
	~Board();

	BoardField getField(int i, int j);
	BoardField getNeighbour(int i, int j, GomokuDirection dir);
	BoardField getNeighbour(pair<int, int> pos, GomokuDirection dir);
	BoardField getNeighbour(int i, int j, GomokuDirection dir, int length);
	BoardField getNeighbour(pair<int, int> pos, GomokuDirection dir, int length);
	pair<int, int> getCoordinates(int i, int j, GomokuDirection dir);
	pair<int, int> getCoordinates(pair<int, int> pos, GomokuDirection dir);
	pair<int, int> getCoordinates(int i, int j, GomokuDirection dir, int length);
	pair<int, int> getCoordinates(pair<int, int> pos, GomokuDirection dir, int length);
	bool isConnected(int i, int j, int radius);
};

