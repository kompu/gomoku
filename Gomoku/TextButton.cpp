#include "TextButton.h"

TextButton::TextButton(shared_ptr<Application> context, sf::Vector2f pos, string str, int fontSize) : Controller(context, pos)
{
	shared_ptr<sf::Font> font = context->getFont();
	text.setFont(*font);
	text.setString(str);
	text.setCharacterSize(fontSize);
	text.setColor(COLOR_CATSKILL);
	text.setStyle(sf::Text::Bold);
	text.setPosition(pos.x - text.getGlobalBounds().width / 2, pos.y);
}

TextButton::~TextButton()
{
}

void TextButton::draw()
{
	shared_ptr<sf::RenderWindow> window = context->getWindow();
	sf::Vector2f mouse(sf::Mouse::getPosition(*window));
	if (contains(mouse))
		text.setColor(COLOR_BLUE);
	else
		text.setColor(COLOR_CATSKILL);
	window->draw(text);
}

bool TextButton::contains(sf::Vector2f mouse)
{
	return text.getGlobalBounds().contains(mouse);
}

string TextButton::getString()
{
	return text.getString();
}