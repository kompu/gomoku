#include "ComputerPlayer.h"

ComputerPlayer::ComputerPlayer(BoardField playerColor) : Player(playerColor)
{
	turn = (playerColor == BoardField::BLACK);
	waiting = false;
	srand(time(NULL));
}

ComputerPlayer::~ComputerPlayer()
{
}

bool ComputerPlayer::isWaitingForLocalMove()
{
	return false;
}

void ComputerPlayer::setOnPlayerListener(OnPlayerListener* listener)
{
	this->listener = listener;
}

pair<int, int> ComputerPlayer::getLastMove()
{
	return lastMove;
}

void ComputerPlayer::makeMove(pair<int, int> move)
{
	board.makeMove(move.first, move.second);
	turn = true;
}

pair<int, int> callFromThread(GameState& board)
{
	auto move = MinMaxTree::nextMove(board);
	return move;
}

void ComputerPlayer::clock()
{
	if (turn)
	{
		turn = false;
		waiting = true;
		task = async(callFromThread, board);
	}
	else if (waiting)
	{
		auto status = task.wait_for(chrono::milliseconds(1));
		if (status == future_status::ready)
		{
			lastMove = task.get();
			board.makeMove(lastMove.first, lastMove.second);
			if (listener != nullptr)
			{
				listener->onPlayerMove(this);
			}
			waiting = false;
		}
	}
}