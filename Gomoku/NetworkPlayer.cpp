#include "NetworkPlayer.h"

NetworkPlayer::NetworkPlayer(BoardField playerColor, unique_ptr<sf::TcpSocket> connectionSocket) : Player(playerColor)
{
	this->connectionSocket = move(connectionSocket);
	this->connectionSocket->setBlocking(false);
	awakeInterval = 0;
}

NetworkPlayer::~NetworkPlayer()
{
	connectionSocket->disconnect();
}

bool NetworkPlayer::isWaitingForLocalMove()
{
	return false;
}

void NetworkPlayer::setOnPlayerListener(OnPlayerListener* listener)
{
	this->listener = listener;
}

pair<int, int> NetworkPlayer::getLastMove()
{
	return lastMove;
}

void NetworkPlayer::makeMove(pair<int, int> move)
{
	sf::Packet packet;
	packet << (sf::Uint8)PacketMessage::MOVE_MESSAGE << (sf::Uint8)move.first << (sf::Uint8)move.second;
	if (connectionSocket->send(packet) != sf::Socket::Done)
	{
		throw NetworkException("Connection error.");
	}
}

void NetworkPlayer::clock()
{
	sf::Packet packet;
	sf::Uint8 message, first, second;
	string str;
	if (connectionSocket->receive(packet) == sf::Socket::Done)
	{
		packet >> message;
		switch ((PacketMessage)message)
		{
		case PacketMessage::MOVE_MESSAGE:
			packet >> first >> second;
			lastMove = make_pair(first, second);
			if (listener != nullptr)
			{
				listener->onPlayerMove(this);
			}
			break;
		case PacketMessage::STRING_MESSAGE:
			packet >> str;
			if (listener != nullptr)
			{
				listener->onPlayerMessage(this, str);
			}
			break;
		}
	}
	if (++awakeInterval == AWAKE_INTERVAL)
	{
		awakeInterval = 0;
		packet << (sf::Uint8)PacketMessage::AWAKE_MESSAGE;
		if (connectionSocket->send(packet) != sf::Socket::Done)
		{
			throw NetworkException("Connection error");
		}
	}
}

void NetworkPlayer::sendMessage(string msg)
{
	sf::Packet packet;
	packet << (sf::Uint8)PacketMessage::STRING_MESSAGE << msg;
	if (connectionSocket->send(packet) != sf::Socket::Done)
	{
		throw NetworkException("Connection error.");
	}
}