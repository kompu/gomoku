#pragma once
#include <string>
#include <memory>
#include <SFML/Graphics.hpp>
#include "Enums.h"

using namespace std;

const int INF = 1000000001;
const int SCREEN_WIDTH = 900;
const int SCREEN_HEIGHT = 600;
const string APP_TITLE = "Gomoku";
const int BOARD_SIZE = 15;
const int BROADCAST_PORT = 53126;
const int GAME_PORT = 53127;
const int AWAKE_INTERVAL = 100;

const sf::Color COLOR_TAN = sf::Color(0xE8, 0xD0, 0xA9);
const sf::Color COLOR_NOMAD = sf::Color(0xB7, 0xAF, 0xA3);
const sf::Color COLOR_JET = sf::Color(0xC1, 0xDA, 0xD6);
const sf::Color COLOR_CATSKILL = sf::Color(0xF5, 0xFA, 0xFA);
const sf::Color COLOR_BLUE = sf::Color(0xAC, 0xD1, 0xE9);
const sf::Color COLOR_GOTHIC = sf::Color(0x6D, 0x92, 0x9B);

