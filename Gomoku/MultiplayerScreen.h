#pragma once
#include "MenuScreen.h"
#include "OnClickListener.h"
#include "TextButton.h"
#include "ScreenManager.h"
#include "HostScreen.h"
#include "MultiplayerSearchScreen.h"
#include "MultiplayerConnectScreen.h"

class MultiplayerScreen : public MenuScreen, public OnDialogResult
{
protected:
	shared_ptr<TextButton> searchButton;
	shared_ptr<TextButton> connectToIpButton;
	shared_ptr<TextButton> hostButton;
	shared_ptr<TextButton> cancelButton;
public:
	MultiplayerScreen(shared_ptr<Application> context);
	~MultiplayerScreen();

	void onClick(Controller* controller) override;
	void onDialogResult(string title, string ret) override;
};

