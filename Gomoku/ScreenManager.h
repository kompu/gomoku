#pragma once
#include <vector>
#include "View.h"
#include "MainMenuScreen.h"

class ScreenManager : public View
{
protected:
	shared_ptr<sf::RenderWindow> window;
	vector<unique_ptr<Screen>> screens;

	void render();
public:
	ScreenManager(shared_ptr<Application> context);
	~ScreenManager();
	void run();
	void addScreen(Screen* screen);
	void removeLastScreen();
	void removePreviousScreen();
	void removeAllScreens();
};

